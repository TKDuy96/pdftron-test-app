import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  PermissionsAndroid,
  BackHandler,
  Alert,
  Button,
  ScrollView,
} from 'react-native';

// @ts-ignore
import {DocumentView, RNPdftron, Config} from 'react-native-pdftron';
import mockAnnotatons from './mockData/annotations';
// import io from 'socket.io-client';
// import axios from 'axios';

// const documentId = 'abc123';
// const SERVER_URL = '167.71.203.225';
// const socket = io(`http://${SERVER_URL}:4000`);

type Props = {};

export class PDFTronAnotherScreen extends Component<Props> {
  private PDFTronRef: null;

  state = {
    permissionGranted: Platform.OS === 'ios',
  };

  constructor(props: Readonly<Props>) {
    super(props);

    RNPdftron.initialize('Insert commercial license key here after purchase');
    RNPdftron.enableJavaScript(true);

    this.PDFTronRef = null;
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.requestStoragePermission().catch(() => {});
    }
  }

  // connectWebsocket = () => {
  //   socket.emit('userJoinRoom', {
  //     userName: 'ha',
  //     room: documentId,
  //   });
  //   socket.on('annotationUpdated', this.onAnnotationUpdated);
  // };

  // onAnnotationUpdated = (data) => {
  //   console.log('=== onAnnotationUpdated ===');
  //   this.PDFTronRef?.importAnnotationCommand(data.xfdf, false);
  // };

  // getAnnotationId = (action, xfdf) => {
  //   let annotationId;
  //   if (action === 'add' || action === 'modify') {
  //     let temp = xfdf.slice(xfdf.indexOf('name') + 6, -1);
  //     annotationId = temp.slice(0, temp.indexOf(' ') - 1);
  //   } else if (action === 'delete') {
  //     let begin = xfdf.indexOf('<id>');
  //     let end = xfdf.indexOf('</id>');
  //     annotationId = xfdf.slice(begin + 4, end);
  //   }

  //   return annotationId;
  // };

  // onExportAnnotationCommand = (object) => {
  //   console.log('onExportAnnotationCommand');
  //   const {action, xfdfCommand} = object;
  //   const anntaionId = this.getAnnotationId(action, xfdfCommand);
  //   socket.emit('annotationChanged', {
  //     annotationId: anntaionId,
  //     documentId: documentId,
  //     xfdf: xfdfCommand,
  //     action,
  //   });
  // };

  // getAnnotation = () => {
  //   axios
  //     .get(`http://${SERVER_URL}:3000/api/annotations`, {
  //       params: {
  //         documentId: documentId,
  //       },
  //     })
  //     .then((res) => {
  //       let temp = [];
  //       const annotations = res.data;

  //       annotations.map((a) => {
  //         temp.push(a.xfdf);
  //       });
  //       console.log(temp);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };

  initAnnotations() {
    mockAnnotatons.map((data) => {
      //this.PDFTronRef?.importAnnotationCommand(data, false);

      // @ts-ignore
      this.PDFTronRef.importAnnotationCommand(data, true);
    });
  }

  // @ts-ignore
  async requestStoragePermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.setState({
          permissionGranted: true,
        });
        console.log('Storage permission granted');
      } else {
        this.setState({
          permissionGranted: false,
        });
        console.log('Storage permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  onLeadingNavButtonPressed = () => {
    console.log('leading nav button pressed');
    if (Platform.OS === 'ios') {
      Alert.alert(
        'App',
        'onLeadingNavButtonPressed',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: true},
      );
    } else {
      BackHandler.exitApp();
    }
  };

  onDocumentLoaded = () => {
    this.initAnnotations();
    // this.getAnnotation();
    // this.connectWebsocket();
    // if (this.PDFTronRef) {
    //
    //   // @ts-ignore
    //   this.PDFTronRef.getPageCount().then((totalPageNumber) => {
    //     console.log('Success getPageCount', totalPageNumber)
    //   }).catch(( error: any) => {
    //     console.log('Fail getPageCount')
    //     console.log(error)
    //   })
    // }
  };

  onAnnotationMenuPress = ({ annotations }: any) => {
    // Alert.alert('alo', 'are you sure?', [
    //   {text: 'Cancel', onPress: () => {}, style: 'cancel'},
    //   {
    //     text: 'OK',
    //     onPress: () => {
    //       // @ts-ignore
    //       this.PDFTronRef && this.PDFTronRef.deleteAnnotations([
    //         {
    //           id: annotations[0].id,
    //           pageNumber: annotations[0].pageNumber,
    //         },
    //       ])
    //         .then((alo: any) => {
    //           console.log('alo', alo);
    //         })
    //         .catch((error: any) => {
    //           console.log('error', error);
    //         });
    //     },
    //   },
    // ]);
  };

  selectedTool = (toolName: string) => {
    switch (toolName) {
      case 'freeHand':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateFreeHand);
        break;
      case 'freeText':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateFreeText);
        break;
      case 'rectangle':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateRectangle);
        break;
      case 'ellipse':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateEllipse);
        break;
      case 'comment':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateSticky);
        break;
      case 'highlight':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateTextHighlight);
        break;
      case 'underline':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateTextUnderline);
        break;

      default:
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.textSelect);
        break;
    }
  };

  renderButton = () => {
    return (
      <View style={{width: '25%'}}>
        <ScrollView>
          <Button
            title={'Free Hand'}
            onPress={() => {
              this.selectedTool('freeHand');
            }}
          />

          <Button
            title={'Free Text'}
            onPress={() => {
              this.selectedTool('freeText');
            }}
          />

          <Button
            title={'Rectangle'}
            onPress={() => {
              this.selectedTool('rectangle');
            }}
          />

          <Button
            title={'Ellipse'}
            onPress={() => {
              this.selectedTool('ellipse');
            }}
          />

          <Button
            title={'comment tool'}
            onPress={() => {
              this.selectedTool('comment');
            }}
          />

          <Button
            title={'Highlight'}
            onPress={() => {
              this.selectedTool('highlight');
            }}
          />

          <Button
            title={'Underline'}
            onPress={() => {
              this.selectedTool('underline');
            }}
          />

          <Button
            title={'Turn off tool'}
            onPress={() => {
              this.selectedTool('');
            }}
          />
        </ScrollView>
      </View>
    );
  };

  onPageChanged = (event) => {
    console.log("change page")
    console.log(event)
  }

  render() {
    if (!this.state.permissionGranted) {
      return (
        <View style={styles.container}>
          <Text>Storage permission required.</Text>
        </View>
      );
    }

    const path =
      'https://kadira-test.s3.us-east-2.amazonaws.com/01-C02-SS-001.pdf';

    return (
      <View style={{flex: 1, flexDirection: 'row'}}>
        {this.renderButton()}
        <DocumentView
          ref={(c: null) => (this.PDFTronRef = c)}
          document={path}
          showLeadingNavButton={true}
          leadingNavButtonIcon={
            Platform.OS === 'ios'
              ? 'ic_close_black_24px.png'
              : 'ic_arrow_back_white_24dp'
          }
          fitMode={Config.FitMode.Zoom}
          layoutMode={Config.LayoutMode.Single}
          onLeadingNavButtonPressed={this.onLeadingNavButtonPressed}
          annotationMenuItems={[
            Config.AnnotationMenu.style,
            Config.AnnotationMenu.delete,
            Config.AnnotationMenu.editInk
          ]}
          onDocumentLoaded={this.onDocumentLoaded}
          topToolbarEnabled={false}
          bottomToolbarEnabled={false}
          selectAnnotationAfterCreation={false}
          continuousAnnotationEditing={true}
          overrideAnnotationMenuBehavior={[Config.AnnotationMenu.delete]}
          onAnnotationMenuPress={this.onAnnotationMenuPress}
          onPageChanged={this.onPageChanged}
          // onExportAnnotationCommand={this.onExportAnnotationCommand}
          collabEnabled={true}
          currentUser={'ha'}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
