import React, {Component} from 'react'
import {
  Alert,
  BackHandler,
  Button,
  PermissionsAndroid,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View
} from 'react-native'
// @ts-ignore
import {Config, DocumentView, RNPdftron} from 'react-native-pdftron'
import mockAnnotations from './mockData/annotations'
type Props = {};

export class PDFTronScreen extends Component<Props> {
  private PDFTronRef: null;
  private path : string;


  constructor(props: Readonly<Props>) {
    super(props)

    RNPdftron.initialize('Insert commercial license key here after purchase');
    RNPdftron.enableJavaScript(true);
    this.state = {
      permissionGranted: Platform.OS === 'ios',
      filePath: 'https://pdftron.s3.amazonaws.com/downloads/pl/PDFTRON_mobile_about.pdf'

    };

    this.PDFTronRef = null
  }

  componentDidMount() {
    if (Platform.OS === 'android') {
      this.requestStoragePermission().catch(() => {
      })
    }
  }

  initAnnotations() {
    mockAnnotations.map((data) => {
      // @ts-ignore
      this.PDFTronRef.importAnnotationCommand(data, false).then(() => {
        console.log('Xong')
      });
    });
  }

  // @ts-ignore
  async requestStoragePermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
      )
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.setState({
          permissionGranted: true
        })
        console.log('Storage permission granted')
      } else {
        this.setState({
          permissionGranted: false
        })
        console.log('Storage permission denied')
      }
    } catch (err) {
      console.warn(err)
    }
  }

  onLeadingNavButtonPressed = () => {
    console.log('leading nav button pressed')
    if (Platform.OS === 'ios') {
      Alert.alert(
        'App',
        'onLeadingNavButtonPressed',
        [{text: 'OK', onPress: () => console.log('OK Pressed')}],
        {cancelable: true}
      )
    } else {
      BackHandler.exitApp()
    }
  }

  onDocumentLoaded = () => {
    this.initAnnotations();
  };
  onDocumentError = (error) => {
    console.log('error', error)
  }

  onAnnotationMenuPress = ({annotations}: any) => {
    // Alert.alert('alo', 'are you sure?', [
    //   {text: 'Cancel', onPress: () => {}, style: 'cancel'},
    //   {
    //     text: 'OK',
    //     onPress: () => {
    //       // @ts-ignore
    //       this.PDFTronRef && this.PDFTronRef.deleteAnnotations([
    //         {
    //           id: annotations[0].id,
    //           pageNumber: annotations[0].pageNumber,
    //         },
    //       ])
    //         .then((alo: any) => {
    //           console.log('alo', alo);
    //         })
    //         .catch((error: any) => {
    //           console.log('error', error);
    //         });
    //     },
    //   },
    // ]);
  }

  selectedTool = (toolName: string) => {
    switch (toolName) {
      case 'freeHand':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateFreeHand)
        break
      case 'freeText':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateFreeText)
        break
      case 'polygon':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreatePolygon)
        break
      case 'ellipse':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateEllipse)
        break
      case 'comment':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateSticky)
        break
      case 'highlight':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateTextHighlight)
        break
      case 'underline':
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.annotationCreateTextUnderline)
        break

      default:
        // @ts-ignore
        this.PDFTronRef && this.PDFTronRef.setToolMode(Config.Tools.textSelect)
        break
    }
  }

  changePath = () => {
    this.setState({
      filePath: 'https://s3.amazonaws.com/pdftron/files/pdf/linearization/50mb-linearized.pdf'
            },() => {
              console.log("change document path")
              console.log(this.state.filePath)
            })

  }

  renderButton = () => {
    return (
      <View style={{width: '25%'}}>
        <ScrollView>
          <Button
            title={'Free Hand'}
            onPress={() => {
              this.selectedTool('freeHand')
            }}
          />

          <Button
            title={'Free Text'}
            onPress={() => {
              this.selectedTool('freeText')
            }}
          />

          <Button
            title={'Polygon'}
            onPress={() => {
              this.selectedTool('polygon')
            }}
          />

          <Button
            title={'Ellipse'}
            onPress={() => {
              this.selectedTool('ellipse')
            }}
          />

          <Button
            title={'comment tool'}
            onPress={() => {
              this.selectedTool('comment')
            }}
          />

          <Button
            title={'Highlight'}
            onPress={() => {
              this.selectedTool('highlight')
            }}
          />

          <Button
            title={'Underline'}
            onPress={() => {
              this.selectedTool('underline')
            }}
          />

          <Button
            title={'Turn off tool'}
            onPress={() => {
              this.selectedTool('')
            }}
          />

          <Button
            title={'Save (CommitTool)'}
            onPress={() => {
              // @ts-ignore
              this.PDFTronRef && this.PDFTronRef.commitTool()
            }}
          />
          <Button
            title={'Change path'}
            onPress={() => {
              this.changePath();
            }}
          />
        </ScrollView>
      </View>
    )
  }

  render() {
    if (!this.state.permissionGranted) {
      return (
        <View style={styles.container}>
          <Text>Storage permission required.</Text>
        </View>
      )
    }

    // const path =
    //   'https://dev-api.workpacks.com/api/document?fs_url=https://dev-fs.workpacks.com/workpacks-fs&project_id=88&cwa=CWA%2001&cwp=01-C03-AP&iwp=01-C03-AP-010&path=07%20-%20IWPs';

    const path = 'https://kadira-test.s3.us-east-2.amazonaws.com/01-C02-SS-001.pdf'

    return (
      <View style={{flex: 1, flexDirection: 'row'}}>
        {this.renderButton()}
       <DocumentView
          ref={(c: null) => (this.PDFTronRef = c)}
          document={path}
          // customHeaders={{
          //   Authorization:
          //     'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxOTQsInVzZXJuYW1lIjoidGFpa2grMCIsImlhdCI6MTU5MTc3NTIwNiwiZXhwIjoxNTk0MzY3MjA2fQ.NBAys-Fk4QObcQmJX0PT4PzWqvz__oMtJMke5z11kqM',
          // }}
          showLeadingNavButton={true}
          leadingNavButtonIcon={
            Platform.OS === 'ios'
              ? 'ic_close_black_24px.png'
              : 'ic_arrow_back_white_24dp'
          }
          fitMode={Config.FitMode.FitPage}
          layoutMode={Config.LayoutMode.Single}
          onLeadingNavButtonPressed={this.onLeadingNavButtonPressed}
          followSystemDarkMode={false}
          annotationMenuItems={[
            Config.AnnotationMenu.style,
            Config.AnnotationMenu.delete,
            Config.AnnotationMenu.editInk
          ]}
          onDocumentLoaded={this.onDocumentLoaded}
          onDocumentError={this.onDocumentError}
          topToolbarEnabled={false}
          bottomToolbarEnabled={false}
          selectAnnotationAfterCreation={false}
          continuousAnnotationEditing={true}
          // overrideAnnotationMenuBehavior={[Config.AnnotationMenu.delete]}
          onAnnotationMenuPress={this.onAnnotationMenuPress}
          onExportAnnotationCommand={(event) => { console.log('event', event) }}
          collabEnabled={true}
          currentUser={'ha'}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  }
})
