import React, { FunctionComponent } from 'react'
import { StyleSheet, Text, View } from 'react-native'

export const HomeScreen: FunctionComponent = () => {
  return (
    <View style={ styles.container }>
      <Text style={ styles.title }>
        This is Home Screen
      </Text>
    </View>
  )
}

const styles = StyleSheet.create( {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold'
  }
} )
