import React, { FunctionComponent } from 'react'
import { RootNavigation } from './navigation/RootNavigation'

export const App: FunctionComponent = () => {
  return (
    <RootNavigation/>
  )
}
