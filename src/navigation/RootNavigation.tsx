import React, { FunctionComponent } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { BottomTabBar } from './BottomTabBar'

const Stack = createStackNavigator()

export const RootNavigation: FunctionComponent = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={'BottomTabBar'}>
        <Stack.Screen name="BottomTabBar" component={ BottomTabBar }/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}
