import React, { FunctionComponent } from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { HomeScreen } from '../HomeScreen'
import { PDFTronScreen } from '../PDFTronScreen'
import { PDFTronAnotherScreen } from '../PDFTronAnotherScreen'

const Tab = createBottomTabNavigator();

export const BottomTabBar: FunctionComponent = () => {
  return (
    <Tab.Navigator initialRouteName={'HomeScreen'}>
      <Tab.Screen name="HomeScreen" component={HomeScreen} />
      <Tab.Screen name="PDFTronScreen" component={PDFTronScreen} />
      <Tab.Screen name="PDFTronAnotherScreen" component={PDFTronAnotherScreen} />
    </Tab.Navigator>
  );
}
